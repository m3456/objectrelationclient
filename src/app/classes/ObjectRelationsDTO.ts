export class ObjectRelationsDTO{
    constructor(
        public idObjectRelations:number=-1,
        public idObject1:number=-1,
        public idObject2:number=-1,
        public idRelationsType:number=-1,
        public description:string="",
        public isActive:boolean=true,
         ){}   
}