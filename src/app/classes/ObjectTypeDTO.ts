export class ObjectTypeDTO{
    constructor(
        public idObjectTypes:number=-1,
        public nameObjectTypes:string="",
        public icon:string=""
         ){} 
}