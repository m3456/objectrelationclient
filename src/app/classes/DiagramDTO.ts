export class diagramDTO
{
    constructor(
        public idObjectRelations:number=-1,
        public idObject1?:number,
        public idObject2?:number,
        public idRelationsType?:number,
        public description?:string,
        public isActive?:boolean
         ){}
    
}