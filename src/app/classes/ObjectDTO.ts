export class ObjectDTO{
   constructor(
       public idObject:number=-1,
       public idGuid:number=-1,
       public idObjectTypes:number=-1,
       public nameObject:string="",
       public imgObject:string="",
       public isActive:boolean=true,
        ){} 
}