import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CheckboxModule } from 'primeng/checkbox';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ObjectTypeComponent } from './components/object-type/object-type.component';
import { ObjectComponent } from './components/object/object.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { RelationTypeComponent } from './components/relation-type/relation-type.component';
import { ObjectRelationComponent } from './components/object-relation/object-relation.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'primeng/fileupload';
import { InspectorComponent } from './components/inspector/inspector.component';
import { DiagramComponent } from './components/diagram/diagram.component';
import { GojsEditComponent } from './components/gojs-edit/gojs-edit.component';
import { AddObjectTypeComponent } from './components/add/add-object-type/add-object-type.component';
import { AddObjectComponent } from './components/add/add-object/add-object.component';
import { AddRelationTypeComponent } from './components/add/add-relation-type/add-relation-type.component';
import { AddObjectRelationComponent } from './components/add/add-object-relation/add-object-relation.component';
import { TryoutComponent } from './components/tryout/tryout.component';
import { IconListComponent } from './components/icon-list/icon-list.component';
import {ListboxModule} from 'primeng/listbox';
import { AddNewNodeComponent } from './components/add-new-node/add-new-node.component';



@NgModule({
  declarations: [
    AppComponent,
    ObjectTypeComponent,
    ObjectComponent,
    RelationTypeComponent,
    ObjectRelationComponent,
    NavbarComponent,
    FileUploadComponent,
    InspectorComponent,
    DiagramComponent,
    GojsEditComponent,
    AddObjectTypeComponent,
    AddObjectComponent,
    AddRelationTypeComponent,
    AddObjectRelationComponent,
    TryoutComponent,
    IconListComponent,
    AddNewNodeComponent,
    // RelationsTypesListComponent
  ],
  imports: [
    ListboxModule,
    BrowserModule,
   AppRoutingModule,
    HttpClientModule,
    CheckboxModule,
    TableModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
