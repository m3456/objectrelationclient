import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router) { }
  active1: Boolean
  active2: boolean
  active3: boolean
  active4: boolean
  ngOnInit() {
    this.active1= true
    this.active2 = false
    this.active3= false
    this.active4= false
  }

}
