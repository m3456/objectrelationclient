import { Component, OnInit } from '@angular/core';
import { RelationTypeService } from 'src/app/services/relation-type.service';
import { Router } from '@angular/router';
import { RelationsTypeDTO } from 'src/app/classes/RelationsTypeDTO';
import { ObjectTypeService } from 'src/app/services/object-type.service';

@Component({
  selector: 'app-relation-type',
  templateUrl: './relation-type.component.html',
  styleUrls: ['./relation-type.component.css']
})
export class RelationTypeComponent implements OnInit {
  fileData: any
  editing: boolean = false
  constructor(private relationTypeService: RelationTypeService, private objectTypeService: ObjectTypeService, private router: Router) { }
  clonedRelTs: { [s: string]: RelationsTypeDTO; } = {};
  load: boolean;
  deleting: boolean = false


  ngOnInit() {
    this.relationTypeService.GetAllRelationType().subscribe((data: RelationsTypeDTO[]) => {
      console.log(data);
      this.relationTypeService.relationTypeList = data;
      this.load = true
    }, (err) => alert(err.message))
  }
  onRowEditInit(RelT: RelationsTypeDTO) {
    this.clonedRelTs[RelT.idRelationsType] = { ...RelT };
  }
  onRowEditSave(RelT: RelationsTypeDTO) {
    if (RelT.nameRelationsType) {
      delete this.clonedRelTs[RelT.idRelationsType];
      this.relationTypeService.UpdateRelationsType(RelT, this.fileData).subscribe(
        data => console.log("EDIT SUCCESS"),
        err => console.log("edit error")
      )
    }
    else {
    }
  }
  Delete(RelT: RelationsTypeDTO) {
    //     this.deleting = false;
    // debugger
    //     this.objectTypeService.DeleteIpPremission(RelT).subscribe(data => { this.objectTypeService.objectTypeList.splice(this.objectTypeService.objectTypeList.indexOf(RelT), 1) }
    //     )
  }
  onRowEditCancel(RelT: RelationsTypeDTO, index: number) {
    this.relationTypeService.relationTypeList[index] = this.clonedRelTs[RelT.idRelationsType];
    delete this.clonedRelTs[RelT.idRelationsType];

  }
  add() {
    this.relationTypeService.isShow = true
    this.router.navigate(['navbar/addRelationType'])
  }
  // debugg()
  // {
  //   alert("In here")
  //   this.objectTypeService.tryout().subscribe(data=>{alert(data)},err=>{alert(JSON.stringify(err))});
  // }
  fileProgress(fileInput: any, rt: RelationsTypeDTO) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview(rt);
  }

  preview(rt: RelationsTypeDTO) {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      rt.icon = <string>reader.result;
    }
  }
  f(s: string) {
    return "Images/" + s
  }
}
