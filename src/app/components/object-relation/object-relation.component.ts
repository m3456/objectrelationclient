import { Component, OnInit } from '@angular/core';
import { ObjectRelationService } from 'src/app/services/object-relation.service';
import { Router } from '@angular/router';
import { RelationTypeService } from 'src/app/services/relation-type.service';
import { ObjectRelationsDTO } from 'src/app/classes/ObjectRelationsDTO';
import { RelationsTypeDTO } from 'src/app/classes/RelationsTypeDTO';
import { ObjectService } from 'src/app/services/object.service';
import { ObjectDTO } from 'src/app/classes/ObjectDTO';
import { ObjectTypeService } from 'src/app/services/object-type.service';
import { ObjectTypeDTO } from 'src/app/classes/ObjectTypeDTO';

@Component({
  selector: 'app-object-relation',
  templateUrl: './object-relation.component.html',
  styleUrls: ['./object-relation.component.css']
})
export class ObjectRelationComponent implements OnInit {

  editing: boolean = false
  constructor(private objectRelationService: ObjectRelationService, private router: Router, private relationTypeService: RelationTypeService, private objectService: ObjectService, private objectTypeService: ObjectTypeService) { }
  clonedObjRels: { [s: string]: ObjectRelationsDTO; } = {};
  load: boolean;
  deleting: boolean = false
  currList1: Array<ObjectDTO>
  currList2: Array<ObjectDTO>
  helping1: ObjectTypeDTO
  helping2: ObjectTypeDTO

  ngOnInit() {
    this.objectRelationService.GetAllObjectRelation().subscribe((data: ObjectRelationsDTO[]) => {
      console.log(data)
      this.objectRelationService.objectRelationsList = data;
      this.relationTypeService.GetAllRelationType().subscribe((data: RelationsTypeDTO[]) => {
        console.log(data)
        this.relationTypeService.relationTypeList = data;
        this.objectTypeService.GetAllObjectType().subscribe((data: ObjectTypeDTO[]) => {
          console.log(data)
          this.objectTypeService.objectTypeList = data;
          this.objectService.GetAllObject().subscribe((data: ObjectDTO[]) => {
          console.log(data)
          this.objectService.objectList = data;
          this.load = true
        })
      })
      })
    },
    (err)=>{alert(err.message)})

  }
  getNameType(objRel: ObjectRelationsDTO) {
    debugger
    var x = this.relationTypeService.relationTypeList.find(o => o.idRelationsType == objRel.idRelationsType).nameRelationsType
    return x
  }
  getObject1(objRel: ObjectRelationsDTO) {
    debugger
    var x = this.objectService.objectList.find(o => o.idObject == objRel.idObject1)
    return x
  }
  getObject2(objRel: ObjectRelationsDTO) {
    debugger
    var x = this.objectService.objectList.find(o => o.idObject == objRel.idObject2)
    return x
  }
  getNameObjType(obj: ObjectDTO) {
    debugger
    var x = this.objectTypeService.objectTypeList.find(o => o.idObjectTypes == obj.idObjectTypes)
    return x.nameObjectTypes
  }
  ngAfterViewInit() {

  }
  onRowEditInit(ObjRel: ObjectRelationsDTO) {
    debugger
    var h1 = this.objectService.objectList.find(o => o.idObject == ObjRel.idObject1)
    this.helping1 = this.objectTypeService.objectTypeList.find(o => o.idObjectTypes == h1.idObjectTypes)
    this.currList1 = this.objectService.objectList.filter(o => o.idObjectTypes == this.helping1.idObjectTypes)

    var h2 = this.objectService.objectList.find(o => o.idObject == ObjRel.idObject2)
    this.helping2 = this.objectTypeService.objectTypeList.find(o => o.idObjectTypes == h2.idObjectTypes)
    this.currList2 = this.objectService.objectList.filter(o => o.idObjectTypes == this.helping2.idObjectTypes)
    this.clonedObjRels[ObjRel.idObjectRelations] = { ...ObjRel };

  }
  // onChange1(e: Event) {
  //   this.currList1 = this.objectService.objectList.filter(o => o.idObjectTypes == this.helping1.idObjectTypes)
  // }
  // onChange2(e: Event) {
  //   this.currList2 = this.objectService.objectList.filter(o => o.idObjectTypes == this.helping2.idObjectTypes)
  // }
  // x() {
  //   this.objectService.objectList.filter(x => x.idObjectTypes == this.helping2.idObjectTypes)
  // }
  onRowEditSave(ObjRel: ObjectRelationsDTO) {
    if (ObjRel.idObject1) {
      delete this.clonedObjRels[ObjRel.idObjectRelations];
      this.objectRelationService.UpdateObjectRelations(ObjRel).subscribe(
        data => console.log("EDIT SUCCESS"),
        err => console.log("edit error")
      )
    }
    else {
    }
  }
  onRowEditCancel(ObjRel: ObjectRelationsDTO, index: number) {
    debugger
    this.objectRelationService.objectRelationsList[index] = this.clonedObjRels[ObjRel.idObjectRelations];
    delete this.clonedObjRels[ObjRel.idObjectRelations];
  }
  Delete(ObjRel: ObjectRelationsDTO) {
    this.deleting = false;
    ObjRel.isActive = false
    this.objectRelationService.UpdateObjectRelations(ObjRel).subscribe()
  }
  add() {
    this.objectRelationService.isShow = true
    this.router.navigate(['navbar/addObjectRelation'])
  }

}
