import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectRelationComponent } from './object-relation.component';

describe('ObjectRelationComponent', () => {
  let component: ObjectRelationComponent;
  let fixture: ComponentFixture<ObjectRelationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectRelationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectRelationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
