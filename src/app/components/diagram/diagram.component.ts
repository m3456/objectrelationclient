

import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import * as go from 'gojs'
import { RelationsTypeDTO } from 'src/app/classes/RelationsTypeDTO';
import { DiagramService } from '../../services/diagram.service'
import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { RelationTypeService } from 'src/app/services/relation-type.service';
const $ = go.GraphObject.make;
@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.css']
})
export class DiagramComponent implements AfterViewInit, OnInit {
  ngOnInit(): void {
    this.DiagramService.GetDiagramByObjectId().subscribe(
      (data) =>{ debugger;alert(data); this.DiagramService.model = new go.TreeModel(data)},
      (err) => {console.log(err)}
      )
    
  }
  nodefirst: go.Part;
  icons: Array<string> = new Array<string>()
  public diagram: go.Diagram = null;
  isShow: boolean = false
  relationsType: RelationsTypeDTO
  count: number;
  constructor(private DiagramService: DiagramService, private route: Router, private relationTypeService: RelationTypeService) { }
  chooseIcon() {
    debugger;
    this.isShow = true;
  }
  focus() {

    this.diagram.commandHandler.zoomToFit();

  }
  center() {
    // document.getElementById('centerRoot').addEventListener('click', ()=> {
    this.diagram.scale = 1;
    this.diagram.commandHandler.scrollToPart(this.diagram.findNodeForKey(1));
    // }
    // );
  }

  public ngAfterViewInit() {
    //צריך את המשתנה הזה?
    this.isShow = false;
    this.icons = [
      "../../assets/icons/house.png",
      "../../assets/icons/gmail.png",
      "../../assets/icons/protest.png",
      "../../assets/icons/screen.png",
      "../../assets/icons/text.png",
    ]
    var roundedRectangleParams = {
      parameter1: 2,  // set the rounded corner
      spot1: go.Spot.TopLeft, spot2: go.Spot.BottomRight  // make content go all the way to inside edges of rounded corners
    };
    this.diagram = $(go.Diagram, 'myDiagramDiv',
      {

        layout:
          $(go.TreeLayout,
            {
              isOngoing: true,
              treeStyle: go.TreeLayout.StyleLastParents,
              arrangement: go.TreeLayout.ArrangementHorizontal,
              // properties for most of the tree:
              angle: 90,
              layerSpacing: 35,
              // properties for the "last parents":
              alternateAngle: 90,
              alternateLayerSpacing: 35,
              alternateAlignment: go.TreeLayout.AlignmentBus,
              alternateNodeSpacing: 20
            }),
        'undoManager.isEnabled': true
      }
    );

    // define the Node template
    this.diagram.nodeTemplate =
      $(go.Node, 'Auto',

        // for sorting, have the Node.text be the data.name
        new go.Binding('text', 'name'),

        // bind the Part.layerName to control the Node's layer depending on whether it isSelected
        new go.Binding('layerName', 'isSelected', function (sel) { return sel ? 'Foreground' : ''; }).ofObject(),

        // define the node's outer shape
        $(go.Shape, 'Rectangle',
          {
            name: 'SHAPE', fill: '#333333', stroke: null, strokeWidth: 3.5,
            // set the port properties:
            portId: '', fromLinkable: true, toLinkable: true, cursor: 'pointer'
          },
          new go.Binding('fill', '', function (node) {
            // modify the fill based on the tree depth level
            const levelColors = ['#AC193D', '#2672EC', '#8C0095', '#5133AB',
              '#008299', '#D24726', '#008A00', '#094AB2'];
            let shape = node.findObject('SHAPE');
            const dia: go.Diagram = node.diagram;
            if (dia && dia.layout.network) {
              dia.layout.network.vertexes.each(function (v: go.TreeVertex) {
                if (v.node && v.node.key === node.data.key) {
                  const level: number = v.level % (levelColors.length);
                  var color = levelColors[level];
                  if (shape) shape.stroke = $(go.Brush, "Linear", { 0: color, 1: go.Brush.lightenBy(color, 0.05), start: go.Spot.Left, end: go.Spot.Right });
                }
              });
            }
            return shape;
          }).ofObject()
        ),
        $(go.Panel, 'Horizontal',

          $(go.Picture,
            {
              name: 'Picture',
              desiredSize: new go.Size(70, 70),
              margin: 1.5,
            },
            new go.Binding('source', 'key', function (key) {
              if (key < 0 || key > 16) return ''; // There are only 16 images on the server
              return 'assets/hs' + key + '.jpg';
            })
          ),
          // define the panel where the text will appear
          $(go.Panel, 'Table',
            {
              maxSize: new go.Size(150, 999),
              margin: new go.Margin(6, 10, 0, 3),
              defaultAlignment: go.Spot.Left
            },
            $(go.RowColumnDefinition, { column: 2, width: 4 }),
            $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the name
              {
                row: 0, column: 0, columnSpan: 5,
                font: '12pt Segoe UI,sans-serif',
                editable: true, isMultiline: false,
                minSize: new go.Size(10, 16)
              },
              new go.Binding('text', 'name').makeTwoWay()),
            $(go.TextBlock, 'Title: ', { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
              { row: 1, column: 0 }),
            $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
              {
                row: 1, column: 1, columnSpan: 4,
                editable: true, isMultiline: false,
                minSize: new go.Size(10, 14),
                margin: new go.Margin(0, 0, 0, 3)
              },
              new go.Binding('text', 'title').makeTwoWay()),
            $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
              { row: 2, column: 0 },
              new go.Binding('text', 'key', function (v) { return 'ID: ' + v; })),
            $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
              { name: 'boss', row: 2, column: 3 }, // we include a name so we can access this TextBlock when deleting Nodes/Links
              new go.Binding('text', 'parent', function (v) { return 'Boss: ' + v; })),
            $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the comments
              {
                row: 3, column: 0, columnSpan: 5,
                font: 'italic 9pt sans-serif',
                wrap: go.TextBlock.WrapFit,
                editable: true,  // by default newlines are allowed
                minSize: new go.Size(10, 14)
              },
              new go.Binding('text', 'comments').makeTwoWay()),
          )  // end Table Panel
        ) // end Horizontal Panel

      );  // end Node
    this.diagram.nodeTemplate.contextMenu =
      $("ContextMenu",
        $("ContextMenuButton",
          $(go.TextBlock, "Add new node"),
          {
            click: (e, obj) => {
              // var node = obj.part;
              const node = this.diagram.selection.first();
              if (node === null) return;
              this.DiagramService.addnodeClicked(node);
            }
          }
        ),
        $("ContextMenuButton",
          $(go.TextBlock, "Remove Role"),
          {
            click: function (e, obj) {
              // reparent the subtree to this node's boss, then remove the node
              const node = this.diagram.selection.first();
              if (node !== null) {
                this.diagram.startTransaction("reparent remove");
                var chl = node.findTreeChildrenNodes();
                // iterate through the children and set their parent key to our selected node's parent key
                while (chl.next()) {
                  var emp = chl.value;
                  this.diagram.model.setParentKeyForNodeData(emp.data, node.findTreeParentNode().data.key);
                }
                // and now remove the selected node itself
                this.diagram.model.removeNodeData(node.data);
                this.diagram.commitTransaction("reparent remove");
              }
            }
          }
        ),
        $("ContextMenuButton",
          $(go.TextBlock, "Remove Department"),
          {
            click: (e, obj) => {
              this.nodefirst = this.diagram.selection.first();
              this.removePartTree(this.nodefirst)

              // remove the whole subtree, including the node itself

            }
          }
        ),
        $("ContextMenuButton",
          $(go.TextBlock, "Toggle Assistant"),
          {
            click: function (e, obj) {
              // remove the whole subtree, including the node itself
              const node = this.diagram.selection.first();

              if (node !== null) {
                this.diagram.startTransaction("toggle assistant");
                this.diagram.model.setDataProperty(node.data, "isAssistant", !node.data.isAssistant);
                this.diagram.layout.invalidateLayout();
                this.diagram.commitTransaction("toggle assistant");
              }
            }
          }
        )
      );
    this.diagram.nodeTemplate.selectionAdornmentTemplate =
      $(go.Adornment, "Spot",
        $(go.Panel, "Auto",
          $(go.Shape, "RoundedRectangle", roundedRectangleParams,
            { fill: null, stroke: "#7986cb", strokeWidth: 3 }),
          $(go.Placeholder)  // a Placeholder sizes itself to the selected Node
        ),
        // the button to create a "next" node, at the top-right corner
        $("Button",
          {
            alignment: go.Spot.TopRight,
            click: (e, obj) => {
              const node = this.diagram.selection.first();
              if (node === null) return;
              e.handled = true;
              this.expandNode(node);
              this.DiagramService.addnodeClicked(node);
            } // this function is defined below
          },
          $(go.Shape, "PlusLine", { width: 6, height: 6 })
        ),
        $("Button",
          {
            alignment: go.Spot.RightCenter,
            click: (e, obj) => {
              debugger
              this.nodefirst = this.diagram.selection.first();
              this.removePartTree(this.nodefirst)
            }
          },
          $(go.Shape, "MinusLine", { width: 6, height: 6 })
        ),
        $("Button",
          {
            alignment: go.Spot.TopLeft,
            click: (e, obj) => {
              debugger
              this.nodefirst = this.diagram.selection.first();
              this.editText(this.nodefirst)
            }
          },
          $(go.TextBlock, "t",
            { font: "bold 10pt sans-serif", desiredSize: new go.Size(6, 6), textAlign: "center" })
        )
      );
    this.diagram.model = this.DiagramService.model;
    // when the selection changes, emit event to app-component updating the selected node
    this.diagram.addDiagramListener('ChangedSelection', (e) => {
      const node = this.diagram.selection.first();
      this.DiagramService.setSelectedNode(node);

    });
    this.count = this.diagram.nodes.count
    // this.diagram.addDiagramListener('ObjectDoubleClicked', (e) => {
    //   const node = this.diagram.selection.first();
    //   this.addnodeClicked.emit(node);

    // });
    this.diagram.linkTemplate =
      $(go.Link,  // the whole link panel
        $(go.Shape,  // the link shape
          { stroke: "black" }),
        $(go.Shape,  // the arrowhead
          { toArrow: "standard", stroke: null }
        ),
        $(go.Panel, "Auto",
          $(go.Shape,  // the label background, which becomes transparent around the edges
            {
              fill: $(go.Brush, "Radial", { 0: "rgb(240, 240, 240)", 0.3: "rgb(240, 240, 240)", 1: "rgba(240, 240, 240, 0)" }),
              stroke: null
            }),

          $(go.Picture,
            {
              name: 'Picture',
              desiredSize: new go.Size(22, 22),
              click: (e) => { this.isShow = true }
              // {this.route.navigate(['diagram/relationsTypesList'])}
            },
            new go.Binding('source', 'icon',
              //  (icon)=> { return new go.Binding('icon', 'name'),}
              // return this.relationTypeService.icon;
            )
          ),

        )
      );


  }
  removePartTree(node: any) {
    debugger
    for (let index = 1; index <= this.count; index++) {
      if (this.DiagramService.model.findNodeDataForKey(index) != undefined) {
        if (this.DiagramService.model.findNodeDataForKey(index).parent === node.key) {
          this.removePartTree(this.DiagramService.model.findNodeDataForKey(index))
        }
      }
    }
    if (node.key == this.nodefirst.key) {
      this.DiagramService.deletnodeClickedfirst(node);
    }
    else {
      this.DiagramService.deletnodeClicked(node);

    }
  }

  expandNode(node: any) {
    var diagram = node.diagram;
    diagram.startTransaction("CollapseExpandTree");
    // this behavior is specific to this incrementalTree sample:
    var data = node.data;
    if (!data.everExpanded) {
      // only create children once per node
      diagram.model.setDataProperty(data, "everExpanded", true);
      var numchildren = this.createSubTree(data);
      if (numchildren === 0) {  // now known no children: don't need Button!
        node.findObject('TREEBUTTON').visible = false;
      }
    }
    // this behavior is generic for most expand/collapse tree buttons:
    if (node.isTreeExpanded) {
      diagram.commandHandler.collapseTree(node);
    } else {
      diagram.commandHandler.expandTree(node);
    }
    diagram.commitTransaction("CollapseExpandTree");
    this.diagram.zoomToFit();
  }
  createSubTree(data: any) {
    var numchildren = Math.floor(Math.random() * 10);
    if (this.diagram.nodes.count <= 1) {
      return numchildren += 1;  // make sure the root node has at least one child
    }
  }

  // removePartTree(node) {
  //   for (let index = 1; index <= this.count; index++) {
  //     if (this.DiagramService.model.findNodeDataForKey(index) != undefined) {
  //       if (this.DiagramService.model.findNodeDataForKey(index).parent === node.key) {
  //         this.removePartTree(this.DiagramService.model.findNodeDataForKey(index))
  //       }
  //     }
  //   }
  //   if (node.key == this.nodefirst.key) {
  //     this.DiagramService.deletnodeClickedfirst(node);
  //   }
  //   else {
  //     this.DiagramService.deletnodeClicked(node);

  //   }
  // }
  editText(node) {
    //כאן צריך לגשת לשרת ולעדכן נתונים
    this.DiagramService.model.findNodeDataForKey(node.key).name
  }
}
