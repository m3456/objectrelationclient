import { Component, OnInit } from '@angular/core';
import {ObjectService} from '../../services/object.service'
import { ObjectDTO } from 'src/app/classes/ObjectDTO';
@Component({
  selector: 'app-add-new-node',
  templateUrl: './add-new-node.component.html',
  styleUrls: ['./add-new-node.component.css']
})
export class AddNewNodeComponent implements OnInit {
  cars: { label: string; value: string; }[];
  objectList: ObjectDTO[];
  cities: { name: string; code: string; }[];
  selectedCities:{ name: string; code: string; }[];
  constructor(private ObjectService:ObjectService) { }

  ngOnInit() {
    this.cities = [
      {name: 'New York', code: 'NY'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'}
  ];
    // this.ObjectService.GetAllObject().subscribe((data: ObjectDTO[])=>{
    //   this.objectList = data;
    //   })
  }

}
