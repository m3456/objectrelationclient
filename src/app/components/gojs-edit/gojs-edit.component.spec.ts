import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GojsEditComponent } from './gojs-edit.component';

describe('GojsEditComponent', () => {
  let component: GojsEditComponent;
  let fixture: ComponentFixture<GojsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GojsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GojsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
