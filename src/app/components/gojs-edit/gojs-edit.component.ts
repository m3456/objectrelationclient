import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import * as go from 'gojs';
const $=go.GraphObject.make;
@Component({
  selector: 'app-gojs-edit',
  templateUrl: './gojs-edit.component.html',
  styleUrls: ['./gojs-edit.component.css']
})
export class GojsEditComponent implements AfterViewInit,OnInit {
  public myDiagram:go.Diagram=null;
  @Input()
  public nodeDataArray: go.Model; 
  // @Output()
  // public nodeClicked = new EventEmitter();
  // constructor() { }
ngOnInit()
{
console.log("H"+this.nodeDataArray.toJson());
}
  ngAfterViewInit() {
    
   this.myDiagram =
    $(go.Diagram, "myDiagramDiv", // must be the ID or reference to div
      {
        // maxSelectionCount: 1, // users can select only one part at a time
        // validCycle: go.Diagram.CycleDestinationTree, // make sure users can only create trees
        // "clickCreatingTool.archetypeNodeData": { // allow double-click in background to create a new node
        //   name: "(new person)",
        //   title: "",
        //   comments: ""
        // },
        // "clickCreatingTool.insertPart": (loc)=> {  // scroll to the new node
        //   var node = go.ClickCreatingTool.prototype.insertPart.call(this, loc);
        //   if (node !== null) {
        //     this.myDiagram.select(node);
        //     this.myDiagram.commandHandler.scrollToPart(node);
        //     this.myDiagram.commandHandler.editTextBlock(node.findObject("NAMETB"));
        //   }
        //   return node;
        // },
        layout:
          $(go.TreeLayout,
            {
               isOngoing:true,
          treeStyle:go.TreeLayout.StyleLastParents,
          arrangement:go.TreeLayout.ArrangementHorizontal,
          angle:90,
          layerSpacing:35,
          alternateAngle:90,
          alternateLayerSpacing:35,
          alternateAlignment:go.TreeLayout.AlignmentBus,
          alternateNodeSpacing:20,
            }),
        "undoManager.isEnabled": true // enable undo & redo
      });

  }

  // func(){
  // this.myDiagram.addDiagramListener("Modified", (e)=> {
  //   var button = document.getElementById("SaveButton");
  //   if (button) button.c = !this.myDiagram.isModified;
  //   var idx = document.title.indexOf("*");
  //   if (this.myDiagram.isModified) {
  //     if (idx < 0) document.title += "*";
  //   } else {
  //     if (idx >= 0) document.title = document.title.substr(0, idx);
  //   }
  // });
// }
  
// func()
// {
//   this.myDiagram.addDiagramListener("SelectionDeleting", (e)=> {
//     var part = e.subject.first(); // e.subject is the myDiagram.selection collection,
//     // so we'll get the first since we know we only have one selection
//     this.myDiagram.startTransaction("clear boss");
//     if (part instanceof go.Node) {
//       var it = part.findTreeChildrenNodes(); // find all child nodes
//       while (it.next()) { // now iterate through them and clear out the boss information
//         var child = it.value;
//         var bossText = child.findObject("boss"); // since the boss TextBlock is named, we can access it by name
//         if (bossText === null) return;
//         // bossText.text = "";
//         alert("pooppp"+bossText)
//       }
//     } else if (part instanceof go.Link) {
//       var child = part.toNode;
//       var bossText = child.findObject("boss"); // since the boss TextBlock is named, we can access it by name
//       if (bossText === null) return;
//       // bossText.text = "";
//       alert("pooppp"+bossText)
//     }
//     this.myDiagram.commitTransaction("clear boss");
//   });
// }

}
