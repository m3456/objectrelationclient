import { Component, OnInit } from '@angular/core';
import { ObjectTypeService } from 'src/app/services/object-type.service';
import { Router } from '@angular/router';
import { ObjectTypeDTO } from 'src/app/classes/ObjectTypeDTO';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-object-type',
  templateUrl: './add-object-type.component.html',
  styleUrls: ['./add-object-type.component.css']
})
export class AddObjectTypeComponent implements OnInit {

  constructor(private objectTypeService: ObjectTypeService, private location: Location, private router: Router) { }

  newObjectType: ObjectTypeDTO
  objType: FormGroup
  load: boolean
  isShow: boolean
  fileData: any
  previewUrl: any
  get f() { return this.objType.controls; }

  ngOnInit() {
    this.isShow = true
    this.objType = new FormGroup({
      nameObjectType: new FormControl('', Validators.required),
      icon: new FormControl('')
    })
  }
  addObjectType() {
    this.newObjectType = new ObjectTypeDTO(-1, this.f.nameObjectType.value, this.f.icon.value)
    this.objectTypeService.AddObjectType(this.newObjectType,this.fileData).subscribe(
    data => {
    console.log(data);

    // this.objectTypeService.objectTypeList.push(this.newObjectType)
    debugger
    },

    err => console.log("i didn't succeed to add object type"),
    () => 
    this.location.back()
    )
  }
  close() {
    this.objectTypeService.isShow = false
    this.location.back()
  }


  fileProgress(fileInput: any, ) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  preview() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
      debugger
      this.objType.patchValue({ icon: <string>reader.result });
    }
  }

}
