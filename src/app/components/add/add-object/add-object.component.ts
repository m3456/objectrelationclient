import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ObjectService } from 'src/app/services/object.service';
import { ObjectDTO } from 'src/app/classes/ObjectDTO';
import { ObjectTypeService } from 'src/app/services/object-type.service';

@Component({
  selector: 'app-add-object',
  templateUrl: './add-object.component.html',
  styleUrls: ['./add-object.component.css']
})
export class AddObjectComponent implements OnInit {
  constructor(private objectService: ObjectService,private objectTypeService:ObjectTypeService,private location: Location, private router: Router) { }

  newObject: ObjectDTO
  obj: FormGroup
  load: boolean
  isShow: boolean
  fileData:any
  previewUrl:any


  
  get f() { return this.obj.controls; }
  
  ngOnInit() {
    this.isShow = true
    // document.getElementById('id01').style.display = 'block';
    this.obj = new FormGroup({
      nameObject: new FormControl('', Validators.required),
      idObjectTypes: new FormControl(''),
      imgObject: new FormControl('')
    })
  }
  addObject() {
    debugger
    this.newObject = new ObjectDTO(-1,-1, this.f.idObjectTypes.value, this.f.nameObject.value, this.f.imgObject.value,true)
    this.objectService.AddObject(this.newObject,this.fileData).subscribe(
      data => {
        console.log(data);
      
      // this.objectService.objectList.push(this.newObject)
      debugger  

      },
      err => console.log("i didn't succeed to add object type"),
      () => 
      this.location.back()
    )
  }
  close() {
    this.objectService.isShow = false
    this.location.back()
  }


  fileProgress(fileInput: any,) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }
  
  preview() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
  
    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
      debugger      
      this.obj.patchValue({ imgObject: <string>reader.result });
    }
  }
}
