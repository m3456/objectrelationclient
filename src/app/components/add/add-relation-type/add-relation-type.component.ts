import { Component, OnInit } from '@angular/core';
import { RelationTypeService } from 'src/app/services/relation-type.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RelationsTypeDTO } from 'src/app/classes/RelationsTypeDTO';
import { Location } from '@angular/common';


@Component({
  selector: 'app-add-relation-type',
  templateUrl: './add-relation-type.component.html',
  styleUrls: ['./add-relation-type.component.css']
})
export class AddRelationTypeComponent implements OnInit {

  constructor(private relationTypeService: RelationTypeService, private location: Location, private router: Router) { }

  newRelationsType: RelationsTypeDTO
  newRelationsType2: RelationsTypeDTO
  // relType: FormGroup
  load: boolean
  isShow: boolean
  fileData: any
  fileData2: any
  previewUrl: any
  previewUrl2: any
  mutual: boolean = false
  nameRelationType: string = ""
  nameRelationType2: string = ""
  // get f() { return this.relType.controls; }
  // get icon() {
  //   return this.relType.get('icon');
  // }
  ngOnInit() {
    this.isShow = true
    // this.relType = new FormGroup({
    //   nameRelationType: new FormControl('', Validators.required),
    //   icon: new FormControl('')
    // })
  }
  addRelationType() {
    this.newRelationsType = new RelationsTypeDTO(-1, this.nameRelationType, this.previewUrl)
    if (this.mutual = true) {
      this.relationTypeService.AddRelationsType(this.newRelationsType, this.fileData).subscribe(
        data => {
          console.log(data);
          this.relationTypeService.relationTypeList.push(this.newRelationsType)
        },
        err => console.log("i didn't succeed to add object type"),
        () =>
          this.location.back()
      )
    }
    else {
      this.newRelationsType2 = new RelationsTypeDTO(-1, this.nameRelationType2, this.previewUrl2)
      this.relationTypeService.AddRelationsType(this.newRelationsType, this.fileData, this.newRelationsType2, this.fileData2).subscribe(
        data => {
          console.log(data);
          this.relationTypeService.relationTypeList.push(this.newRelationsType)
        },
        err => console.log("i didn't succeed to add object type"),
        () =>
          this.location.back()
      )
    }
  }
  close() {
    this.relationTypeService.isShow = false
    this.location.back()
  }


  fileProgress(fileInput: any, ) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  fileProgress2(fileInput: any, ) {
    this.fileData2 = <File>fileInput.target.files[0];
    this.preview2();
  }

  preview() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
      debugger
      // this.relType.patchValue({ icon: <string>reader.result });
    }

  }
  preview2() {
    // Show preview 
    var mimeType = this.fileData2.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData2);
    reader.onload = (_event) => {
      this.previewUrl2 = reader.result;
      debugger
      // this.relType.patchValue({ icon: <string>reader.result });
    }

  }
}
