import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ObjectService } from 'src/app/services/object.service';
import { ObjectDTO } from 'src/app/classes/ObjectDTO';
import { ObjectRelationService } from 'src/app/services/object-relation.service';
import { ObjectRelationsDTO } from 'src/app/classes/ObjectRelationsDTO';
import { RelationTypeService } from 'src/app/services/relation-type.service';
import { ObjectTypeService } from 'src/app/services/object-type.service';


@Component({
  selector: 'app-add-object-relation',
  templateUrl: './add-object-relation.component.html',
  styleUrls: ['./add-object-relation.component.css']
})
export class AddObjectRelationComponent implements OnInit {

  constructor(private objectService: ObjectService, private relationTypeService: RelationTypeService, private objectRelationService: ObjectRelationService,private objectTypeService:ObjectTypeService , private location: Location, private router: Router) { }

  newobjectRelation: ObjectRelationsDTO
  objRel: FormGroup
  load: boolean
  isShow: boolean
  currList1: Array<ObjectDTO>
  currList2: Array<ObjectDTO>

  get f() { return this.objRel.controls; }

  ngOnInit() {
  // this.relationTypeService.relationTypeList
    this.isShow = true
    // document.getElementById('id01').style.display = 'block';
    this.objRel = new FormGroup({
      idObject1: new FormControl('', Validators.required),
      idObject2: new FormControl('', Validators.required),
      idRelationsType: new FormControl('', Validators.required),
      description: new FormControl('')
    })
    this.currList1 = this.objectService.objectList.filter(o => o.idObjectTypes == 1)
    this.currList2 = this.objectService.objectList.filter(o => o.idObjectTypes == 1)
  }
  addobjectRelation() {
    debugger
    this.newobjectRelation = new ObjectRelationsDTO(-1, this.f.idObject1.value, this.f.idObject2.value, this.f.idRelationsType.value, this.f.description.value, true)
    this.objectRelationService.AddObjectRelations(this.newobjectRelation).subscribe(
    data => {
    console.log(data);

    // this.objectRelationService.objectRelationsList.push(this.newobjectRelation)
    debugger
    },

    err => console.log("i didn't succeed to add object type"),
    () => 
    this.location.back()
    )
  }
  a()
  {
    debugger
  }
  onChange1(e:any){
    this.currList1 = this.objectService.objectList.filter(o => o.idObjectTypes == e.target.selectedIndex+1)
    debugger
    this.objRel.patchValue({ idObject1: e.target.selectedIndex+1 });
  }
  onChange2(e:any){
    debugger
    this.currList2 = this.objectService.objectList.filter(o => o.idObjectTypes == e.target.selectedIndex+1)
    this.objRel.patchValue({ idObject2: e.target.selectedIndex+1 });
  }
  close() {
    this.objectService.isShow = false
    this.location.back()
  }


}
