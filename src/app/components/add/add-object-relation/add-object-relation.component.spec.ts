import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddObjectRelationComponent } from './add-object-relation.component';

describe('AddObjectRelationComponent', () => {
  let component: AddObjectRelationComponent;
  let fixture: ComponentFixture<AddObjectRelationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddObjectRelationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddObjectRelationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
