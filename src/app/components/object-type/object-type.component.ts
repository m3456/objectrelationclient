import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FromEventTarget } from 'rxjs/internal/observable/fromEvent';
import { ObjectTypeService } from 'src/app/services/object-type.service';
import { ObjectTypeDTO } from 'src/app/classes/ObjectTypeDTO';

@Component({
  selector: 'app-object-type',
  templateUrl: './object-type.component.html',
  styleUrls: ['./object-type.component.css']
})
export class ObjectTypeComponent implements OnInit {
  fileData: any
  editing: boolean = false
  constructor(private objectTypeService: ObjectTypeService, private router: Router, private activatedRoute: ActivatedRoute) { }
  clonedObjTs: { [s: string]: ObjectTypeDTO; } = {};
  load: boolean;
  deleting: boolean = false

  ngOnInit() {
    this.objectTypeService.GetAllObjectType().subscribe((data: ObjectTypeDTO[]) => {
      console.log(data);
      this.objectTypeService.objectTypeList = data;
      this.load = true
    })
    debugger
  }
  ngAfterViewInit() {

  }
  onRowEditInit(ObjT: ObjectTypeDTO) {
    this.clonedObjTs[ObjT.idObjectTypes] = { ...ObjT };
  }
  onRowEditSave(ObjT: ObjectTypeDTO) {
    if (ObjT.nameObjectTypes) {
      delete this.clonedObjTs[ObjT.idObjectTypes];
      this.objectTypeService.UpdateObjectType(ObjT, this.fileData).subscribe(
        data => {console.log("EDIT SUCCESS");
        this.objectTypeService.GetAllObjectType().subscribe((data: ObjectTypeDTO[]) => {
          console.log(data);
          this.objectTypeService.objectTypeList = data;})},
        err => console.log("edit error")
      )
    }
    else {
    }
  }
  Delete(ObjT: ObjectTypeDTO) {
    //     this.deleting = false;
    // debugger
    //     this.objectTypeService.DeleteIpPremission(ObjT).subscribe(data => { this.objectTypeService.objectTypeList.splice(this.objectTypeService.objectTypeList.indexOf(ObjT), 1) }
    //     )
  }
  onRowEditCancel(ObjT: ObjectTypeDTO, index: number) {
    this.objectTypeService.objectTypeList[index] = this.clonedObjTs[ObjT.idObjectTypes];
    delete this.clonedObjTs[ObjT.idObjectTypes];

  }
  add() {
    this.objectTypeService.isShow = true
    this.router.navigate(['../addObjectType'], { relativeTo: this.activatedRoute });
    // this.router.navigate(['navbar/addObjectType'])
  }
  // debugg()
  // {
  //   alert("In here")
  //   this.objectTypeService.tryout().subscribe(data=>{alert(data)},err=>{alert(JSON.stringify(err))});
  // }
  fileProgress(fileInput: any, ot: ObjectTypeDTO) {
    this.fileData = <File>fileInput.target.files[0];
    debugger
    this.preview(ot);
  }

  preview(ot: ObjectTypeDTO) {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      ot.icon = <string>reader.result;
    }
  }
  // f(s:string){
  //   return "Images/"+s
  // }

}
