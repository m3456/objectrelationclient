import { Component, OnInit } from '@angular/core';
import { RelationsTypeDTO } from 'src/app/classes/RelationsTypeDTO';
import { RelationTypeService } from 'src/app/services/relation-type.service';
import { ObjectRelationService } from 'src/app/services/object-relation.service';

@Component({
  selector: 'app-icon-list',
  templateUrl: './icon-list.component.html',
  styleUrls: ['./icon-list.component.css']
})
export class IconListComponent implements OnInit {

  constructor(private relationTypesService:RelationTypeService, private objectRelationService:ObjectRelationService) { }
  relationsTypesList:RelationsTypeDTO[]
  
    
  ngOnInit() 
  {
    this.relationTypesService.GetAllRelationType().subscribe(
      (data)=>this.relationsTypesList=data,
      (err)=>console.log(err)
    )
    }
    chooseIcon(x:RelationsTypeDTO)
    {
      // this.objectRelationService.UpdateObjectRelations(x).subscribe(
      //   (data)=>{
      //     console.log("succes")
      // },
      //   (err)=>{console.log("err")}
      
      // )
    
        this.relationTypesService.icon=x.icon;
    }
}
