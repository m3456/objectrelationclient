import { Component, OnInit, Input, HostListener, ElementRef } from '@angular/core';

@Component({
  selector: 'app-file-upload',
  template: `
   <div>
    <span>Choose file</span>
    <span>{{file ? file.name : 'or drag and drop file here' }}</span>
    <input class="file-input" type="file">
  </div>
  `
  // templateUrl: './file-upload.component.html',
  // styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  constructor(private host: ElementRef<HTMLInputElement> ) { }

  ngOnInit() {
  }


}




