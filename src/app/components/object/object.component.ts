import { Component, OnInit } from '@angular/core';
import { ObjectService } from 'src/app/services/object.service';
import { Router } from '@angular/router';
import { ObjectDTO } from 'src/app/classes/ObjectDTO';
import { ObjectTypeService } from 'src/app/services/object-type.service';
import { ObjectTypeDTO } from 'src/app/classes/ObjectTypeDTO';

@Component({
  selector: 'app-object',
  templateUrl: './object.component.html',
  styleUrls: ['./object.component.css']
})
export class ObjectComponent implements OnInit {
  s: string = "../../assets/IMG_0007.JPG"
  fileData: any
  editing: boolean = false
  constructor(private objectService: ObjectService, private router: Router, private objectTypeService: ObjectTypeService) { }
  clonedObjs: { [s: string]: ObjectDTO; } = {};
  load: boolean;
  deleting: boolean = false


  ngOnInit() {
    this.objectService.GetAllObject().subscribe((data: ObjectDTO[]) => {
      console.log(data);
      this.objectService.objectList = data;
      this.objectTypeService.GetAllObjectType().subscribe((data: ObjectTypeDTO[]) => {
        this.objectTypeService.objectTypeList = data;
        this.load = true
      })
    })


  }
  getNameType(obj: ObjectDTO) {
    debugger
    var x = this.objectTypeService.objectTypeList.find(o => o.idObjectTypes == obj.idObjectTypes).nameObjectTypes

    return x
  }

  ngAfterViewInit() {

  }
  onRowEditInit(Obj: ObjectDTO) {

    this.clonedObjs[Obj.idObjectTypes] = { ...Obj };
  }
  onRowEditSave(Obj: ObjectDTO) {
    if (Obj.nameObject) {
      delete this.clonedObjs[Obj.idObject];
      this.objectService.UpdateObject(Obj, this.fileData).subscribe(
        data => console.log("EDIT SUCCESS"),
        err => console.log("edit error")
      )
    }
    else {
    }
  }
  Delete(Obj: ObjectDTO) {
    this.deleting = false;
    Obj.isActive = false
    this.objectService.UpdateObject(Obj, this.fileData).subscribe()
  }
  onRowEditCancel(Obj: ObjectDTO, index: number) {
    this.objectService.objectList[index] = this.clonedObjs[Obj.idObject];
    delete this.clonedObjs[Obj.idObject];

  }
  add() {
    this.objectService.isShow = true
    this.router.navigate(['navbar/addObject'])
  }

  fileProgress(fileInput: any, ob: ObjectDTO) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview(ob);
  }

  preview(ob: ObjectDTO) {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      ob.imgObject = <string>reader.result;
    }
  }
  f(s: string) {
    debugger
    return "Images/" + s
  }
}
