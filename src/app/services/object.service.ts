import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ObjectDTO } from '../classes/ObjectDTO';

@Injectable({
  providedIn: 'root'
})
export class ObjectService {

  objectList: Array<ObjectDTO> = new Array<ObjectDTO>()
  isShow: boolean;
  BASIC_URL = "services/api/Object/"


  constructor(private http: HttpClient) {
    // this.objectList.push(new ObjectDTO(1,1,1,"yosef","../../assets/IMG_0007.JPG",true))
    // this.objectList.push(new ObjectDTO(2,1,1,"eli","../../assets/IMG_0007.JPG",true))
    // this.objectList.push(new ObjectDTO(3,2,2,"mazda3","../../assets/IMG_0007.JPG",true))
  }
  AddObject(Obj: ObjectDTO, fileData: File): Observable<any> {
    const formData = new FormData();
    formData.append('image', fileData);
    formData.append('Object', JSON.stringify(Obj));
    return this.http.post<any>(this.BASIC_URL + "addObject", formData);
  }

  UpdateObject(Obj: ObjectDTO, fileData: File): Observable<any> {
    const formData = new FormData();
    formData.append('image', fileData);
    formData.append('Object', JSON.stringify(Obj));
    return this.http.post<any>(this.BASIC_URL + "UpdateObject", formData);
  }

  GetAllObject(): Observable<any> {
    debugger
    return this.http.get<any>(this.BASIC_URL + "GetAllObject");
  }


  IsExistObject(Object: ObjectDTO): Observable<any> {
    return this.http.get<any>(this.BASIC_URL + "IsExistObjectType/" + Object.idGuid);
  }
}
