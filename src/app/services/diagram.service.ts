import { Injectable } from '@angular/core';
import * as go from 'gojs'
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const $ = go.GraphObject.make;
@Injectable({
  providedIn: 'root'
})
export class DiagramService {
BASIC_URL="services/api/ObjectRelations/"

  deletnodeClickedfirst(event) {
    //לגשת לשרת
    this.model.removeNodeData(event.data)
  }
  public selectedNode = null;
  addnodeClicked(event){
    //לגשת לשרת
    var newemp = {
      'name': "(new person)",
      'title': "",
      'parent':event.key
    };
    this.model.addNodeData(newemp)
  }
  deletnodeClicked(event){
    //לגשת לשרת
     this.model.removeNodeData(event)
   }
 
  public setSelectedNode(node) {
    this.selectedNode = node;
  }
  public model: go.TreeModel = new go.TreeModel(
   [
       {"key":1, "name":"Stella Payne Diaz", "title":"CEO"},
      {"key":2, "name":"Luke Warm", "title":"VP Marketing/Sales", "parent":1,"text":"represent","icon":"../../assets/icons/house.png"},
      {"key":3, "name":"Meg Meehan Hoffa", "title":"Sales", "parent":2,"icon":"../../assets/icons/house.png"},
      {"key":4, "name":"Peggy Flaming", "title":"VP Engineering", "parent":1,"icon":"../../assets/icons/house.png"},
      {"key":5, "name":"Saul Wellingood", "title":"Manufacturing", "parent":4,"icon":"../../assets/icons/house.png"},
      {"key":6, "name":"Al Ligori", "title":"Marketing", "parent":2,"icon":"../../assets/icons/house.png"},
      {"key":7, "name":"Dot Stubadd", "title":"Sales Rep", "parent":3,"icon":"../../assets/icons/house.png"},
      {"key":8, "name":"Les Ismore", "title":"Project Mgr", "parent":5,"icon":"../../assets/icons/house.png"},
      {"key":9, "name":"April Lynn Parris", "title":"Events Mgr", "parent":6,"icon":"../../assets/icons/house.png"},
      {"key":10, "name":"Xavier Breath", "title":"Engineering", "parent":4,"icon":"../../assets/icons/house.png"},
      {"key":11, "name":"Anita Hammer", "title":"Process", "parent":5,"icon":"../../assets/icons/house.png"},
      {"key":12, "name":"Billy Aiken", "title":"Software", "parent":10,"icon":"../../assets/icons/house.png"},
      {"key":13, "name":"Stan Wellback", "title":"Testing", "parent":10,"icon":"../../assets/icons/house.png"},
      {"key":14, "name":"Marge Innovera", "title":"Hardware", "parent":10,"icon":"../../assets/icons/house.png"},
      {"key":15, "name":"Evan Elpus", "title":"Quality", "parent":5,"icon":"../../assets/icons/house.png"},
      {"key":16, "name":"Lotta B. Essen", "title":"Sales Rep", "parent":3,"icon":"../../assets/icons/house.png"}
    ]
  );
  GetDiagramByObjectId():Observable<any> {
    debugger
    var objectID=1
    return this.http.get<any>(this.BASIC_URL + "GetDiagramByObjectId/"+objectID); 
  }
  constructor(private http:HttpClient) { }
}
  