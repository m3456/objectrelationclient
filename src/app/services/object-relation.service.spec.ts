import { TestBed } from '@angular/core/testing';

import { ObjectRelationService } from './object-relation.service';

describe('ObjectRelationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ObjectRelationService = TestBed.get(ObjectRelationService);
    expect(service).toBeTruthy();
  });
});
