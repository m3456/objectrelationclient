import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ObjectRelationsDTO } from '../classes/ObjectRelationsDTO';
import { ObjectDTO } from '../classes/ObjectDTO';

@Injectable({
  providedIn: 'root'
})
export class ObjectRelationService {

  BASIC_URL="services/api/ObjectRelations/"
  objectRelationsList:Array<ObjectRelationsDTO>=new Array<ObjectRelationsDTO>()
  isShow: boolean;
  parameters:Array<any>=new Array<any>()
  constructor(private http: HttpClient) { 
    // this.objectRelationsList.push(new ObjectRelationsDTO(1,1,2,2,"friends",true))
    // this.objectRelationsList.push(new ObjectRelationsDTO(2,1,3,1,"his",true))
  }

  GetAllObjectRelation():Observable<any> {
    return this.http.get<any>(this.BASIC_URL + "GetAllObjectRelation");
  }

  AddObjectRelations(ObjectRelations:ObjectRelationsDTO):Observable<any>
  {
    return this.http.post<any>(this.BASIC_URL + "AddObjectRelations", ObjectRelations);
  }

  GetObjectRelationsByObjectId(object:ObjectDTO):Observable<any>
  {
    return this.http.get<any>(this.BASIC_URL + "GetObjectRelationsByObjectId/"+object.idObject);
  }

  IsExistObjectRelations(ObjectRelations:ObjectRelationsDTO):Observable<any>
  {
    this.parameters=new Array<any>()
    this.parameters.push(ObjectRelations.idObject1)
    this.parameters.push(ObjectRelations.idObject1)
    this.parameters.push(ObjectRelations.idRelationsType)
    return this.http.get<any>(this.BASIC_URL + "IsExistObjectRelations/"+this.parameters);
  }
    UpdateObjectRelations(ObjT: ObjectRelationsDTO):Observable<any> {
    return this.http.post<any>(this.BASIC_URL + "UpdateObjectRelations", ObjT);
  }

}
