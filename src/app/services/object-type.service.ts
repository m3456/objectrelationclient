import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ObjectTypeDTO } from '../classes/ObjectTypeDTO';

@Injectable({
  providedIn: 'root'
})
export class ObjectTypeService {


  objectTypeList:Array<ObjectTypeDTO>=new Array<ObjectTypeDTO>()
  isShow: boolean;
  BASIC_URL="services/api/ObjectType/"
  
  constructor(private http:HttpClient) { 
    // this.objectTypeList.push(new ObjectTypeDTO(1,"person","../../assets/IMG_0007.JPG"))
    // this.objectTypeList.push(new ObjectTypeDTO(2,"car","../../assets/IMG_0007.JPG"))
  }

  UpdateObjectType(ObjectType: ObjectTypeDTO,fileData:File):Observable<any> {
    const formData = new FormData();
     formData.append('image',fileData);
     formData.append('ObjectType',JSON.stringify(ObjectType));
    return this.http.post<any>(this.BASIC_URL + "UpdateObjectType", formData);
  }

  GetAllObjectType():Observable<any> {
    debugger
    return this.http.get<any>(this.BASIC_URL + "GetAllObjectType");
    }



    


  AddObjectType(ObjectType:ObjectTypeDTO,fileData:File):Observable<any>
  {
    const formData = new FormData();
     formData.append('image',fileData);
     formData.append('ObjectType',JSON.stringify(ObjectType));
    return this.http.post<any>(this.BASIC_URL + "AddObjectType", formData);
  }

  IsExistObjectType(ObjectType:ObjectTypeDTO):Observable<any>
  {
    return this.http.get<any>(this.BASIC_URL + "IsExistObjectType/"+ObjectType.nameObjectTypes);
  }
}
