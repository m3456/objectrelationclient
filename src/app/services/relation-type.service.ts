import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { RelationsTypeDTO } from '../classes/RelationsTypeDTO';

@Injectable({
  providedIn: 'root'
})
export class RelationTypeService {
  constructor(private http: HttpClient) {
    // this.relationTypeList.push(new RelationsTypeDTO(1,"own","../../assets/IMG_0007.JPG"))
    // this.relationTypeList.push(new RelationsTypeDTO(2,"friend","../../assets/IMG_0007.JPG"))
  }
  BASIC_URL = "services/api/RelationsType/"
  relationTypeList: Array<RelationsTypeDTO> = new Array<RelationsTypeDTO>()
  isShow: boolean;
  isShowIcons: boolean
  icon: string = null



  GetAllRelationType(): Observable<any> {
    return this.http.get<any>(this.BASIC_URL + "GetAllRelationsType");
  }

  AddRelationsType(RelationsType: RelationsTypeDTO, fileData: File, RelationsType2?: RelationsTypeDTO, fileData2?: File): Observable<any> {
    const formData = new FormData();
    formData.append('image', fileData);
    formData.append('RelationsType', JSON.stringify(RelationsType));
    if (RelationsType2) {
      formData.append('image2', fileData2);
      formData.append('RelationsType2', JSON.stringify(RelationsType2));
    }
    return this.http.post<any>(this.BASIC_URL + "AddRelationsType", formData);
  }

  UpdateRelationsType(RelationsType: RelationsTypeDTO, fileData: File): Observable<any> {
    const formData = new FormData();
    formData.append('image', fileData);
    formData.append('RelationsType', JSON.stringify(RelationsType));
    return this.http.post<any>(this.BASIC_URL + "UpdateRelationsType", formData);
  }

  IsExistRelationsType(RelationsType: RelationsTypeDTO): Observable<any> {
    return this.http.get<any>(this.BASIC_URL + "IsExistObjectType/" + RelationsType.nameRelationsType);
  }
}
