import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ObjectTypeComponent } from './components/object-type/object-type.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ObjectComponent } from './components/object/object.component';
import { RelationTypeComponent } from './components/relation-type/relation-type.component';
import { ObjectRelationComponent } from './components/object-relation/object-relation.component';
import { AddObjectTypeComponent } from './components/add/add-object-type/add-object-type.component';
import { AddObjectComponent } from './components/add/add-object/add-object.component';
import { AddRelationTypeComponent } from './components/add/add-relation-type/add-relation-type.component';
import { AddObjectRelationComponent } from './components/add/add-object-relation/add-object-relation.component';
import { DiagramComponent } from './components/diagram/diagram.component';
import { IconListComponent } from './components/icon-list/icon-list.component';

const routes: Routes = [
  { path: 'navbar', component: NavbarComponent,
    children:[
      {path: 'objectType', component: ObjectTypeComponent},
      {path: 'addObjectType', component: AddObjectTypeComponent},
      {path: 'object', component: ObjectComponent},
      {path: 'addObject', component: AddObjectComponent},
      {path: 'relationType', component: RelationTypeComponent},
      {path: 'addRelationType', component: AddRelationTypeComponent},
      {path: 'objectRelation', component: ObjectRelationComponent},
      {path: 'addObjectRelation', component: AddObjectRelationComponent},
    //   {path: 'diagram', component:DiagramComponent,
    //   children:[{path:'relationsTypesList',component:RelationsTypesListComponent}]
    // }
    ]},
    {path:"iconList",component:IconListComponent},
    {path: '',
    redirectTo: 'navbar/objectType',
    pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
